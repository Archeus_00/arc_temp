#include "splash.hpp"
#include "arc_cpp/config.hpp"
#include "arc_cpp/state.hpp"

namespace temp {
    Splash::Splash(){
        arc::config->load("res/config/logo.config");
        sprite = new arc::Sprite("archeus::logo");
        sprite->scale(0.75);

        SDL_Point bounds = {
            .x = (720 / 2) - (sprite->getBounds().w / 2),
            .y = (480 / 2) - (sprite->getBounds().h / 2)
        };

        sprite->setPos(bounds);
    }

    Splash::~Splash(){
        delete sprite;
    }

    void Splash::update(){}

    void Splash::render(){
        sprite->render();
    }

    namespace splash {
        arc::State *state;

        void init(){
            state = new Splash();
        }

        void deinit(){
            delete state;
        }

        void update(){
            state->update();
        }

        void render(){
            state->render();
        }
    }
}