#pragma once
#include <vector>
#include "arc_cpp/state.hpp"
#include "arc_cpp/sprite.hpp"

namespace temp {
    class Splash : public arc::State {
    public:
        Splash();
        ~Splash();

        void update();
        void render();
        
    private:
        arc::Sprite *sprite;
    };

    namespace splash {
        void init();
        void deinit();

        void update();
        void render();
    }
}