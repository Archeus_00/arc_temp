#include "splash/splash.hpp"
#include "arc_cpp/engine.hpp"
#include "arc_cpp/config.hpp"
#include "arc/engine/engine.h"
#include "arc/engine/state.h"

int main(int argc, char** argv){
    arc::Engine *engine = new arc::Engine();

    temp::splash::init();

    ARC_State state = {
        .updateFn = temp::splash::update,
        .renderFn = temp::splash::render
    };

    ARC_Handler_Add(arc::data->state, &state);

    engine->run();

    temp::splash::deinit();
    return 0;
}